<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function index()
    {
        $order = Order::all();
        $user = Auth::user();
        return view('admin.payment.index', compact('user', 'order'));
    }

    public function detail($id)
    {
        Carbon::setLocale('id');
        $order = order::findOrFail($id);
        $order->event_date = Carbon::parse($order->event_date)->isoFormat('dddd, D MMMM Y') . ', Pukul ' . Carbon::parse($order->event_date)->format('H:i:s') . ' WIB';
        $user = Auth::user();
        return view('admin.payment.detail', compact('user', 'order'));
    }

    public function edit($id, Request $request)
    {
        $vendor = Order::findOrFail($id);
        $vendor->status = $request->status;
        $vendor->save();

        return redirect()->back();
    }
}
