<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorController extends Controller
{
    public function index()
    {
        $vendor = Vendor::all();
        $user = Auth::user();
        return view('admin.vendor.index', compact('user', 'vendor'));
    }

    public function detail($id)
    {
        $vendor = Vendor::findOrFail($id);
        $category = Category::all();
        $user = Auth::user();
        return view('admin.vendor.detail', compact('user', 'vendor', 'category'));
    }

    public function edit($id, Request $request)
    {
        $name = Carbon::now()->format('HisdmY') . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/vendor'), $name);
        $vendor = Vendor::findOrFail($id);
        $vendor->name = $request->name;
        $vendor->number = $request->number;
        $vendor->city = $request->city;
        $vendor->id_category = $request->category;
        $vendor->image = "images/vendor/$name";
        $vendor->save();

        return redirect()->back();
    }
}
