<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            UserSeeder::class,
            AdminSeeder::class,
            ClientSeeder::class,
            VendorSeeder::class,
            ProductSeeder::class,
            OrderSeeder::class,
            VendorWishlistSeeder::class,
            ProductWishlistSeeder::class,
            ReviewSeeder::class,
        ]);
    }
}
