@extends('layouts.simple.master')

@section('title', 'Default')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/owlcarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/rating.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Dashboard</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Dashboard</li>
    <li class="breadcrumb-item active">Admin</li>
@endsection

@section('content')
    <div class="container-fluid" id="homeAdmin">
        <div>
            <div class="row product-page-main p-0">
                <div class="col-xl-4 xl-cs-65 box-col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="product-slider owl-carousel owl-theme" id="sync1">
                                <div class="item"><img src="{{ asset($order->image) }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 xl-100 box-col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="product-page-details">
                                <h3>{{ $order->client->name }}</h3>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-3"><b>Name</b></div>
                                <div class="col-1">:</div>
                                <div class="col-8">{{ $order->client->name }}</div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>Product</b></div>
                                <div class="col-1">:</div>
                                <div class="col-8">{{ $order->product->name }}</div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>Amount</b></div>
                                <div class="col-1">:</div>
                                <div class="col-8">{{ $order->amount }} Packet</div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>Total</b></div>
                                <div class="col-1">:</div>
                                <div class="col-8">Rp. {{ number_format($order->total, 2, ',', '.') }}</div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>Date</b></div>
                                <div class="col-1">:</div>
                                <div class="col-8">{{ $order->event_date }}</div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>Status</b></div>
                                <div class="col-1">:</div>
                                <div class="col-8">{{ $order->status }}</div>
                            </div>
                            <hr>
                            <div class="m-t-15 d-flex justify-content-end">
                                <button type="button" data-bs-toggle="modal" data-bs-target="#modalEdit"
                                    class="btn btn-primary m-r-10" title="">
                                    <i class="fa fa-edit me-1"></i>
                                    Edit
                                </button>
                            </div>

                            <div class="modal fade bd-example-modal-lg" id="modalEdit" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('data_payment_edit', $order->id) }}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data</h5>
                                                <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="mb-3 col-6">
                                                        <label class="col-form-label" for="id_client">Name :</label>
                                                        <input class="form-control" id="id_client" name="id_client"
                                                            type="text" value="{{ $order->client->name }}" disabled>
                                                    </div>
                                                    <div class="mb-3 col-6">
                                                        <label class="col-form-label" for="id_product">Product :</label>
                                                        <input class="form-control" id="id_product" name="id_product"
                                                            type="text" value="{{ $order->product->name }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="col-form-label" for="status">Status :</label>
                                                    <select class="form-select" name="status" id="status">
                                                        @if ($order->status == 'Belum Dibayar')
                                                            <option value="Belum Dibayar" selected>Belum Dibayar</option>
                                                            <option value="Lunas">Lunas</option>
                                                        @else
                                                            <option value="Belum Dibayar">Belum Dibayar</option>
                                                            <option value="Lunas" selected>Lunas</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button"
                                                    data-bs-dismiss="modal">Close</button>
                                                <button class="btn btn-primary" type="submit">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var session_layout = '{{ session()->get('layout') }}';
    </script>
@endsection

@section('script')
    <script src="{{ asset('assets/js/sidebar-menu.js') }}"></script>
    <script src="{{ asset('assets/js/rating/jquery.barrating.js') }}"></script>
    <script src="{{ asset('assets/js/rating/rating-script.js') }}"></script>
    <script src="{{ asset('assets/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/js/ecommerce.js') }}"></script>
@endsection
