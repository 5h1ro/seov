<div class="page-header">
    <div class="header-wrapper row m-0">
        <form class="form-inline search-full col" action="#" method="get">
            <div class="mb-3 w-100">
                <div class="Typeahead Typeahead--twitterUsers">
                    <div class="Typeahead-menu"></div>
                </div>
            </div>
        </form>
        <div class="header-logo-wrapper col-auto p-0">
            <div class="logo-wrapper"><a href="#"><img class="img-fluid"
                        src="{{ asset('assets/images/logo/logo.png') }}" alt=""></a></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="align-center"></i>
            </div>
        </div>
        <div class="left-header col horizontal-wrapper ps-0">
        </div>
        <div class="nav-right col-8 pull-right right-header p-0">
            <ul class="">
                <li>
                    <div class="mode"><i class="fa fa-moon-o"></i></div>
                </li>

                <li class="maximize"><a class="text-dark" href="#!"
                        onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
                <li class="onhover-dropdown p-0 me-0 d-none d-lg-block">
                    <div class="media profile-media">
                        <img class="b-r-10" src="{{ asset('assets/images/dashboard/profile.jpg') }}" alt="">
                        <div class="media-body">
                            @if ($user->role == 'admin')
                                <span>{{ $user->admin->name }}</span>
                            @endif
                            <p class="mb-0 font-roboto">{{ $user->role }} <i class="middle fa fa-angle-down"></i></p>
                        </div>
                    </div>
                    <ul class="profile-dropdown onhover-show-div">
                        <li><a href="{{ route('out') }}"><i data-feather="log-out"> </i><span>Log out</span></a></li>
                    </ul>
                </li>
                <li class="d-block d-md-none d-lg-none">
                    <a href="{{ route('out') }}">
                        <i data-feather="log-out">
                        </i>
                    </a>
                </li>
            </ul>
        </div>
        <script class="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf"></div></script>
        <script class="empty-template" type="text/x-handlebars-template">
            <div class="EmptyMessage">
                                Your search turned up 0 results. This most likely means the backend is down, yikes!
                            </div>
                        </script>
    </div>
</div>
