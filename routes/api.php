<?php

use App\Http\Controllers\api\AuthController as ApiAuthController;
use App\Http\Controllers\api\ClientController;
use App\Http\Controllers\api\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('register', [AuthController::class, 'register']);
// Route::post('login', [AuthController::class, 'login']);
// Route::get('book', [BookController::class, 'book']);
// Route::get('bookall',  [BookController::class, 'bookAuth'])->middleware('jwt.verify');
// Route::get('user', [AuthController::class, 'getAuthenticatedUser'])->middleware('jwt.verify');

// user api
$router->post('/register', [ApiAuthController::class, 'register']);
$router->post('/login', [ApiAuthController::class, 'login']);
$router->post('/forgot', [ApiAuthController::class, 'forgot']);

// client api
$router->get('/client/index', [ClientController::class, 'index'])->middleware('jwt.verify');
$router->put('/client/update', [ClientController::class, 'update'])->middleware('jwt.verify');
$router->get('/client/product-wishlist', [ClientController::class, 'product-wishlist'])->middleware('jwt.verify');
$router->get('/client/vendor-wishlist', [ClientController::class, 'vendor-wishlist'])->middleware('jwt.verify');
$router->get('/client/order', [ClientController::class, 'order'])->middleware('jwt.verify');
$router->post('/client/add-review={id}', [ClientController::class, 'addreview'])->middleware('jwt.verify');
$router->post('/client/add-product-wishlist={id}', [ClientController::class, 'addProductWishlist'])->middleware('jwt.verify');
$router->post('/client/add-vendor-wishlist={id}', [ClientController::class, 'addVendorWishlist'])->middleware('jwt.verify');
$router->delete('/client/delete-product-wishlist={id}', [ClientController::class, 'deleteProductWishlist'])->middleware('jwt.verify');
$router->delete('/client/delete-vendor-wishlist={id}', [ClientController::class, 'deleteVendorWishlist'])->middleware('jwt.verify');

//home api
$router->get('/home/index', [HomeController::class, 'index'])->middleware('jwt.verify');
$router->get('/home/category={id}', [HomeController::class, 'category'])->middleware('jwt.verify');
$router->get('/home/vendor={id}', [HomeController::class, 'vendor'])->middleware('jwt.verify');
$router->get('/home/product={id}', [HomeController::class, 'product'])->middleware('jwt.verify');
$router->post('/home/order={id}', [HomeController::class, 'order'])->middleware('jwt.verify');
$router->get('/home/all-product={id}', [HomeController::class, 'allproduct'])->middleware('jwt.verify');
$router->get('/home/review={id}', [HomeController::class, 'review'])->middleware('jwt.verify');
