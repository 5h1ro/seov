<?php

use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\PaymentController as AdminPaymentController;
use App\Http\Controllers\Admin\VendorController as AdminVendorController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('start');

Route::get('/out', [AuthController::class, 'logout'])->name('out');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

    Route::middleware(['admin'])->group(function () {
        Route::get('admin', [AdminHomeController::class, 'index']);
        //vendor
        Route::get('data_vendor', [AdminVendorController::class, 'index'])->name('data_vendor');
        Route::get('data_vendor_detail={id}', [AdminVendorController::class, 'detail'])->name('data_vendor_detail');
        Route::post('data_vendor_edit={id}', [AdminVendorController::class, 'edit'])->name('data_vendor_edit');
        //payment
        Route::get('data_payment', [AdminPaymentController::class, 'index'])->name('data_payment');
        Route::get('data_payment_detail={id}', [AdminPaymentController::class, 'detail'])->name('data_payment_detail');
        Route::post('data_payment_edit={id}', [AdminPaymentController::class, 'edit'])->name('data_payment_edit');
    });
});
